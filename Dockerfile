FROM memcached:1.6.7-alpine

WORKDIR /app-run

USER root
RUN touch /app-run/memcached.log

USER memcache
EXPOSE 11211

COPY . /app-run/
#ENV MEMCACHED_SIZE 128
ENV PORT="11211"
ENV USER="memcached"
ENV MAXCONN="1024"
ENV MEMCACHED_SIZE="1024"
ENV MAXITEMSIZE=10m

ENTRYPOINT ash /app-run/init.sh
